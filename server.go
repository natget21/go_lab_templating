//http://devdocs.io/
//offline bootstrap documentation

// 00 setting upgomon
//----------------------------------------

// go get github.com/julesguesnon/gomon
// go install github.com/julesguesnon/gomon

// # Edit ~/.bash_profile

// export GOPATH=/Users/$USER/go
// export PATH=$GOPATH/bin:$PATH

// # Reload profile : source ~/.bash_profile

//01 using multiple templates
//----------------------------------------

package main

import (
	"html/template"
	"net/http"
)

//multiple import
// var templ = template.Must(template.ParseFiles("index.html"))
// var templ2 = template.Must(template.ParseFiles("about.html"))

var templ = template.Must(template.ParseFiles("index.html", "about.html"))

// first
func indexHandler(w http.ResponseWriter, r *http.Request) {
	templ.Execute(w, "welcome")
}

//executeTemplate
func aboutHandler(w http.ResponseWriter, r *http.Request) {
	templ.ExecuteTemplate(w, "about.html", "welcome")
}

func main() {
	mux := http.NewServeMux()

	fs := http.FileServer(http.Dir("assets"))
	mux.Handle("/assets/", http.StripPrefix("/assets/", fs))

	mux.HandleFunc("/", indexHandler)
	mux.HandleFunc("/about", aboutHandler)

	http.ListenAndServe(":8080", mux)
}

// using actions
//---------------------------------------

// package main

// import (
// 	"html/template"
// 	"math/rand"
// 	"net/http"
// 	"time"
// )

// var templ = template.Must(template.ParseFiles("con_action.html", "iter_action.html", "set_action.html", "set_more.html"))

// func conditionHandler(w http.ResponseWriter, r *http.Request) {
// 	rand.Seed(time.Now().Unix())
// 	templ.ExecuteTemplate(w, "con_action.html", rand.Intn(10) > 5)
// }

// func iterationHandler(w http.ResponseWriter, r *http.Request) {
// 	templ.ExecuteTemplate(w, "iter_action.html", []string{"mon", "tue", "wen", "thur", "fri"})
// 	//templ.ExecuteTemplate(w, "iter_action.html", nil)
// }

// func setHandler(w http.ResponseWriter, r *http.Request) {
// 	templ.ExecuteTemplate(w, "set_action.html", "hello")
// }

// type Address struct {
// 	Country string
// 	City    string
// }
// type Student struct {
// 	Name    string
// 	Age     string
// 	Address Address
// }

// func setHandler2(w http.ResponseWriter, r *http.Request) {
// 	student := Student{
// 		Name: "student1",
// 		Age:  "20",
// 		Address: Address{
// 			Country: "Ethiopia",
// 			City:    "A.A",
// 		},
// 	}
// 	templ.ExecuteTemplate(w, "set_more.html", student)
// }

// var templ2 = template.Must(template.ParseFiles("incl_action.html", "header.html", "footer.html"))

// func includeHandler(w http.ResponseWriter, r *http.Request) {
// 	templ2.Execute(w, "hello")
// }

// func addCurrency(s string) string {
// 	return "ETB. " + s
// }

// func functionHandler(w http.ResponseWriter, r *http.Request) {
// 	var funcMap = template.FuncMap{"currency": addCurrency}
// 	var templ3 = template.New("functions.html").Funcs(funcMap)
// 	templ3, _ = templ3.ParseFiles("functions.html")
// 	templ3.Execute(w, "100")
// }

// func main() {
// 	mux := http.NewServeMux()

// 	fs := http.FileServer(http.Dir("assets"))
// 	mux.Handle("/assets/", http.StripPrefix("/assets/", fs))

// 	mux.HandleFunc("/conditional", conditionHandler)
// 	mux.HandleFunc("/iteration", iterationHandler)
// 	mux.HandleFunc("/set", setHandler)
// 	mux.HandleFunc("/set2", setHandler2)
// 	mux.HandleFunc("/include", includeHandler)
// 	mux.HandleFunc("/function", functionHandler)
// 	http.ListenAndServe(":8080", mux)
// }

//  activity : movie schedule site having the pages : home / upcoming movies / schedule
//  scedule should be randomly change on each refresh
//  reuse header and footer for all 3 page
//  assuming you recived your data dynamicaly / database  use a struct to pass non static data to your templates
